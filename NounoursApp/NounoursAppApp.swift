import SwiftUI

@main
struct NounoursAppApp: App {
    @StateObject
    var beds: BedsVM = BedsVM(beds: loadBeds())
    
    var body: some Scene {
        WindowGroup {
            BedsView(bedsVM: beds)
        }
    }
}
