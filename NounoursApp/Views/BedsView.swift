import SwiftUI

struct BedsView: View {
    
    //@State public var beds: [Bed]
    @ObservedObject var bedsVM: BedsVM
    
    var body: some View {
        NavigationStack {
            List(bedsVM.beds) { b in
                NavigationLink(destination: BedView(bedVM: BedVM(original: b), bedsVM: bedsVM)) {
                    HStack {
                        Text(b.name)
                        Spacer()
                        Text(String(b.nounours.count))
                    }
                }
            }
        }
    }
}

struct BedsView_Previews: PreviewProvider {
    static var previews: some View {
        BedsView(bedsVM: BedsVM(beds: loadBeds()))
    }
}
