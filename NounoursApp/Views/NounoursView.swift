import SwiftUI

struct NounoursView: View {
    
    //@Binding public var nounours: Nounours
    @ObservedObject var nounoursVM: NounoursVM
    
    var body: some View {
        VStack(alignment: .leading) {
            Label(nounoursVM.model.name, systemImage: "person")
            TextField("name: ", text: $nounoursVM.model.name)
        }
    }
}

struct NounoursView_Previews: PreviewProvider {
    static var previews: some View {
        NounoursView(nounoursVM: NounoursVM(original: loadBeds()[0].nounours[0]))
    }
}
