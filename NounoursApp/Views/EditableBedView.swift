import SwiftUI

struct EditableBedView: View {
    
    @Binding var bedData: Bed.Data
    
    var body: some View {
        TextField("Enter the name here", text: $bedData.name)
    }
}

struct EditableBedView_Previews: PreviewProvider {
    static var previews: some View {
        EditableBedView(bedData: .constant(loadBeds()[0].data))
    }
}
