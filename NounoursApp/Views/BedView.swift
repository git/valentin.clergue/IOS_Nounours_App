import SwiftUI

struct BedView: View {
    
    //@Binding public var bed: Bed
    //@State public var isEdited = false
    @ObservedObject var bedVM: BedVM
    @ObservedObject var bedsVM : BedsVM
    
    var body: some View {
        VStack {
            Text(bedVM.model.name)
            List(bedVM.original.nounours) { n in
                NounoursView(nounoursVM: NounoursVM(original: n))
            }
        }
        .navigationBarTitle(bedVM.model.name)
        .toolbar {
            Button(action: {
                bedVM.isEdited.toggle()
            }) {
                Text("Edit")
            }
        }
        .sheet(isPresented: $bedVM.isEdited) {
            NavigationStack {
                EditableBedView(bedData: $bedVM.model)
                    .toolbar {
                        ToolbarItem(placement: .confirmationAction) {
                            Button("Done") {
                                bedVM.onEdited()
                                bedsVM.update(vm: bedVM)
                            }
                        }
                        ToolbarItem(placement: .cancellationAction) {
                            Button("Cancel") {
                                bedVM.onEdited(isCancel: true)
                            }
                        }
                    }
            }
        }
    }
}

struct BedView_Previews: PreviewProvider {
    static var previews: some View {
        BedView(bedVM: BedVM(original: loadBeds()[0]), bedsVM: BedsVM(beds: loadBeds()))
    }
}
