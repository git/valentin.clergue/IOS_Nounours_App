import Foundation

public struct Nounours: Identifiable {
    public let id: UUID
    public var name: String
    public var hairCount: Int32
    public var date: Date
    
    init(id: UUID, name: String, hairCount: Int32, date: Date) {
        self.id = id
        self.name = name
        self.hairCount = hairCount
        self.date = date
    }
    
    init(name: String, hairCount: Int32, date: Date) {
        self.init(id: UUID(), name: name, hairCount: hairCount, date: date)
    }
}
