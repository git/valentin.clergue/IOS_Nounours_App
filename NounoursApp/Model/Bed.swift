import Foundation

public struct Bed: Identifiable {
    public let id: UUID
    public var name: String
    public var nounours: [Nounours]
    
    init(id: UUID, name: String) {
        self.id = id
        self.name = name
        self.nounours = []
    }
    
    init(name: String) {
        self.init(id: UUID(), name: name)
    }
}

public func loadBeds() -> [Bed] {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy MM dd"
    
    var beds: [Bed] = []
    
    var bed1 = Bed(name: "Star Wars")
    bed1.nounours.append(Nounours(name: "Nounours1", hairCount: 27732, date: formatter.date(from: "2002 01 23")!))
    bed1.nounours.append(Nounours(name: "Nounours2", hairCount: 262, date: formatter.date(from: "2003 04 16")!))
    
    var bed2 = Bed(name: "Indiana Jones")
    bed2.nounours.append(Nounours(name: "Nounours3", hairCount: 232, date: formatter.date(from: "2006 09 02")!))
    bed2.nounours.append(Nounours(name: "Nounours4", hairCount: 24262, date: formatter.date(from: "2007 08 30")!))
    bed2.nounours.append(Nounours(name: "Nounours5", hairCount: 2232362, date: formatter.date(from: "2009 11 13")!))
    
    beds.append(bed1)
    beds.append(bed2)
    
    return beds
}
