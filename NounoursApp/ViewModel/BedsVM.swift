import Foundation

public class BedsVM : ObservableObject {
    @Published var beds: [Bed]
    
    init(beds: [Bed]) {
        self.beds = beds
    }
    
    func update(vm: BedVM) {
        if let i = beds.firstIndex(where: { $0.id == vm.original.id }) {
            beds[i] = vm.original
        }
    }
}
