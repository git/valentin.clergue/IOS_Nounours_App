import Foundation

extension Bed {
    struct Data: Identifiable {
        public let id: UUID
        public var name: String
        public var nounours: [Nounours.Data] = []
    }
    
    var data : Data { Data(id: self.id, name: self.name, nounours: self.nounours.map{ $0.data }) }
    
    mutating func update(from: Data) {
        guard self.id == data.id else { return }
        self.name = data.name
        self.nounours = data.nounours.map{ Nounours(id: $0.id, name: $0.name, hairCount: $0.hairCount, date: $0.date)}
    }
}

public class BedVM : ObservableObject {
    var original: Bed = Bed(name: "")
    @Published var model: Bed.Data
    @Published var isEdited = false
    
    init(original: Bed) {
        self.original = original
        model = original.data
    }
    
    func onEditing() {
        model = original.data
        isEdited = true
    }
    
    func onEdited(isCancel: Bool = false) {
        if (!isCancel) {
            original.update(from: model)
        }
        isEdited = false
        model = original.data
    }
}
