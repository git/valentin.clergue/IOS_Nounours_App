import Foundation

extension Nounours {
    struct Data: Identifiable {
        public let id: UUID
        public var name: String
        public var hairCount: Int32
        public var date: Date
    }
    
    var data: Data {
        Data(id: self.id, name: self.name, hairCount: self.hairCount, date: self.date)
    }
    
    mutating func update(from data: Data) {
        guard data.id == self.id else { return }
        self.name = data.name
        self.hairCount = data.hairCount
        self.date = data.date
    }
}

public class NounoursVM : ObservableObject {
    var original: Nounours = Nounours(id: UUID(), name: "", hairCount: 0, date: Date())
    @Published var model: Nounours.Data
    @Published var isEdited = false
    
    init(original: Nounours) {
        self.original = original
        model = original.data
    }
    
    func onEditing() {
        model = original.data
        isEdited = true
    }
    
    func onEdited(isCancel: Bool = false) {
        if (!isCancel) {
            original.update(from: model)
        }
        isEdited = false
    }
}
